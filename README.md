# Download Android SDK #
The SDK is huge (about 6 GiB). Please **DO NOT CLONE** from Gitlab directly (It will fail due to network flow limit).

We have provided a tar ball of the .git directory at:

* [Baidu Pan](http://pan.baidu.com/s/1o7Cdilk#list/path=%2FPublic%2FDevBoard%2FFirefly-RK3399%2FSource%2FAndroid6.0&parentPath=%2FPublic%2FDevBoard)

Please check the md5 checksum before proceeding:
```
#!shell
$ md5sum /path/to/Firefly-RK3399_Android6.0_git_20170218.tar.gz
f7a49eec73fbec71d19b0e81ea5c6558  Firefly-RK3399_Android6.0_git_20170218.tar.gz
```

If it is correct, uncompress it:
```
#!shell
mkdir -p ~/proj/firefly-rk3399
cd ~/proj/firefly-rk3399
tar xzvf /path/to/Firefly-RK3399_Android6.0_git_20170218.tar.gz
git reset --hard
```
From now on, you can pull updates from gitlab:
```
#!shell
git pull gitlab Firefly_RK3399:Firefly_RK3399
```

Please visit the following website for more details of our Firefly-RK3399 development board. Thank you.

  http://en.t-firefly.com/en/firenow/Firefly_RK3399

# 下载 Android SDK #

SDK 非常巨大（约 6 GiB），请 **不要直接从 Gitlab 下载** 仓库源码（由于流量限制的原因会出错）。

我们提供了 .git 目录的打包，放到云盘上以供下载：

* [百度云盘](http://pan.baidu.com/s/1o7Cdilk#list/path=%2FPublic%2FDevBoard%2FFirefly-RK3399%2FSource%2FAndroid6.0&parentPath=%2FPublic%2FDevBoard)

下载完成后先验证一下 MD5 码：
```
#!shell
$ md5sum /path/to/Firefly-RK3399_Android6.0_git_20170218.tar.gz
f7a49eec73fbec71d19b0e81ea5c6558  Firefly-RK3399_Android6.0_git_20170218.tar.gz
```

确认无误后，就可以解压：
```
#!shell
mkdir -p ~/proj/firefly-rk3399
cd ~/proj/firefly-rk3399
tar xzvf /path/to/Firefly-RK3399_Android6.0_git_20170218.tar.gz
git reset --hard
```

之后就可以从 gitlab 处获取更新的提交，保持同步:
```
#!shell
git pull gitlab Firefly_RK3399:Firefly_RK3399
```

想了解更多我们 Firefly-RK3399 开发板的详情，请浏览以下网址，谢谢！

  http://www.t-firefly.com/zh/firenow/Firefly-rk3399
